object Singleton{
    init {
        println("Singleton invocado")
    }
    var variableName =" Soy una variable"
    fun printVarName(){
        println(variableName)
    }
}

fun main(args:Array<String>) {

    Singleton.printVarName()
    Singleton.variableName ="Tengo un nuevo nombre"
     var a = A()
  }

class A {
    init {
        println("Clase metodo iniciada . propiedad de variableName : ${Singleton.variableName}")
        Singleton.printVarName()
    }
}
